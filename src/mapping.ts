import { BigInt } from "@graphprotocol/graph-ts"
import {
  Contract,
  OwnershipTransferred,
  Transaction
} from "../generated/Contract/Contract"
import { ExampleEntity } from "../generated/schema"

export function handleTransaction(event: Transaction): void {
  let entity = ExampleEntity.load(event.transaction.from.toHex())

  // Entities only exist after they have been saved to the store;
  // `null` checks allow to create entities on demand
  if (entity == null) {
    entity = new ExampleEntity(event.transaction.from.toHex())

    // Entity fields can be set using simple assignments
    entity.count = BigInt.fromI32(0)
  }

  // Entities can be written to the store with `.save()`
  entity.save()
}

export function sum(a: BigInt, b: i32): BigInt{
  return BigInt.fromI32(a.toI32() + b)
}