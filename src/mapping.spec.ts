import {sum} from "./mapping"
import {BigInt} from "@graphprotocol/graph-ts";

export function sumI32(a: i32, b: i32): i32{
  return a + b
}

describe("example", () => {

  it("sum", () => {
    let result = sumI32(1, 2)
    expect<i32>(result).toBe(3, "1 + 2 is 3");
  });

  it("sumBigInt", () => {
    let result = sum(BigInt.fromI32(1), 2)
    expect<BigInt>(result).toBe(BigInt.fromI32(3), "1 + 2 is 3");
  });

});
